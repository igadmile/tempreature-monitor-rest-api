from django.urls import path
from .views import TemphumidityView


urlpatterns = [
    path('get/', TemphumidityView.as_view()),
]
