from django.db import models


class Temphumidity(models.Model):
    timedate = models.DateTimeField(auto_now=True)
    temperature = models.DecimalField(max_digits=6, decimal_places=2)
    humidity = models.DecimalField(max_digits=6, decimal_places=2)

    def __str__(self):
        return str(self.humidity)
