from rest_framework.generics import ListAPIView
from .serializers import TemphumiditySerializers
from .models import Temphumidity


class TemphumidityView(ListAPIView):
    queryset = Temphumidity.objects.all()
    serializer_class = TemphumiditySerializers
