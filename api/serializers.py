from rest_framework import serializers
from .models import Temphumidity


class TemphumiditySerializers(serializers.ModelSerializer):
    class Meta:
        model = Temphumidity
        fields = ('id', 'timedate', 'temperature', 'humidity')
